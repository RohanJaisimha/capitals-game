# Capitals Game

* Guess what capital I'm thinking of. 
* The user guesses the name of a capital, and the program tells you the distance between your guess and the capital you're supposed to guess. 
* Keep going until you get it!
* Hard mode: no zooming in on the map
* Uses Python3 and [Flask](https://flask.palletsprojects.com/en/1.1.x/)