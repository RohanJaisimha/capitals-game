/*
 * Calculate the distance between the city 
 * and the target using Haversine Formula
 */
function calculateDistance(city) {
    const PI = 3.141592653589793; // PI
    const RADIUS = 6371.0; // Radius of earth in kilometres
    const NUMBER_OF_KILOMETRES_IN_A_MILE = 1.60934;

    let lat1 = city["Latitude"];
    let lon1 = city["Longitude"];
    let lat2 = target["Latitude"];
    let lon2 = target["Longitude"]

    let x1 = lat2 - lat1;
    let dLat = x1 * PI / 180;
    let x2 = lon2 - lon1;
    let dLon = x2 * PI / 180;
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1 * PI / 180) * Math.cos(lat2 * PI / 180) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let dist_km = RADIUS * c;
    let dist_mi = dist_km / NUMBER_OF_KILOMETRES_IN_A_MILE;

    return [dist_km.toFixed(3), dist_mi.toFixed(3)]; // round to 3 decimal places
}