from flask import Flask, render_template
import json
import numpy as np

application = Flask(__name__)


def getData(file_name):
    fin = open(file_name, "r")
    data = json.load(fin)
    fin.close()
    return data


def getWeights(data):
    weights = [data[i]["Population"] for i in range(len(data))]
    weights = np.array(weights) / sum(weights)
    return weights


world_data = getData("./data_files/world_capitals_data.json")
world_weights = getWeights(world_data)

usa_data = getData("./data_files/usa_capitals_data.json")
usa_weights = getWeights(usa_data)


@application.route("/")
def home():
    return render_template("home.html")


@application.route("/world")
def world():
    target = np.random.choice(world_data, p=world_weights)
    return render_template("world.html", target=target, data=world_data)


@application.route("/usa")
def usa():
    target = np.random.choice(usa_data, p=usa_weights)
    return render_template("usa.html", target=target, data=usa_data)


if __name__ == "__main__":
    application.run(host="0.0.0.0", debug=True)
